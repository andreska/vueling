
let before = require('cucumber').Before;
let setDefaultTimeout = require('cucumber').setDefaultTimeout;

before(function() {
    setDefaultTimeout(browser.params.timeout * 1000);
    browser.driver.manage().timeouts().setScriptTimeout(browser.params.timeout * 10000);
});
