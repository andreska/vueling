'use strict';
let Utils = function () {
    let expect = require('chai').use(require('chai-as-promised')).expect;
    let EC = protractor.ExpectedConditions;
    let self = this;

// -------------- WAITS ---------------

    // Wait the given element to be present x time
    self.waitElement = function (el, time) {
        if(time === undefined){
            time = 10000;
        }
        browser.wait(EC.presenceOf(el), time, 'Element taking too long to appear');
    };
    // Wait the given element to not be present x time
    self.waitNoElement = function (el, time) {
        if(time === undefined){
            time = 10000;
        }
        browser.wait(EC.not(EC.presenceOf(el)), time, 'Element taking too long to appear');
    };
    // Wait the given element to be clickable x time
    self.waitForClickable = function(el, time) {
        self.waitElement(el, time);
        browser.wait(protractor.until.elementIsVisible(el), time);
        return browser.wait(protractor.until.elementIsEnabled(el), time);
    };
    // Wait the given element to be clickable x time and then click it
    self.clickWhenClickable = function(el, time) {
        self.waitForClickable(el,time);
        return el.click();
    };

// -------------- EXPECTS ---------------

    self.checkCurrentUrl = function (text) {
        return expect(browser.getCurrentUrl()).to.eventually.contain(text);
    };

// -------------- OTHERS ---------------

    self.moveToElement = function (el) {
        browser.actions().mouseMove(el).perform();
    };

};
module.exports = new Utils();
