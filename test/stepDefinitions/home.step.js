'use strict';
let expect = require('chai').use(require('chai-as-promised')).expect;
let Given = require('cucumber').Given;
let When = require('cucumber').When;
let utils = require('../common/utils');
let homePage = require('../pageObjects/home.po.js');

Given(/^Open browser and go to home page$/, function() {
    homePage.getHomePage();
    return utils.checkCurrentUrl('vueling.com');
});

When(/^Login with user ([^"]*) and pass ([^"]*)$/, function(email, password) {
    utils.waitElement(homePage.userInput);
    homePage.openMenuClubAvios();
    homePage.setUser(email);
    homePage.setPassword(password);
    homePage.clickRememberMe();
    homePage.clickLogin();
    utils.waitElement(homePage.errorMsg);
    return utils.checkCurrentUrl('MyVuelingLoginScreen');
});

When(/^Return to home$/, function() {
    homePage.clickHome();
    // utils.waitElement(homePage.tabSearch);
    return expect(homePage.tabSearch.isPresent()).to.eventually.be.true;
});

When(/^Set ([^"]*) as origin$/, function (origin) {
    homePage.setOrigin(origin);
    return expect(homePage.inputOrigin.getAttribute('value')).to.eventually.equal(origin)
});

When(/^Set ([^"]*) as destination$/, function (destination) {
    homePage.setDestination(destination);
    return expect(homePage.inputDestination.getAttribute('value')).to.eventually.contain(destination)
});

When(/^Search$/, function () {
    homePage.search();
    return expect(homePage.avail.isPresent()).to.eventually.be.true;
});


