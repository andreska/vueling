exports.config = {
    directConnect: true,
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--start-maximized'
            ]
        }
    },
    specs: ['features/*.feature'],
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    ignoreUncaughtExceptions: true,
    cucumberOpts: {
        require: [
            '*/**/*.step.js',
            '*/**/hooks.js'
        ],
        keepAlive: false,
        strict: true,
        tags: []
    },
    params: {
        baseUrl: '',
        url: 'https://www.vueling.com/es',
        testlink: false,
        buildName: 'example',
        log: false,
        timeout: 120
    },
    onPrepare: function() {
        browser.waitForAngularEnabled(false);
    }
};
