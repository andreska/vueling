'use strict';
let HomePage = function() {
    let self = this;
    let BASE_URL = browser.params.url;

    // header
    self.btnHome = $('.vy-header_main_brand');
    self.btnAccessNoLogedUser = $('.noLogedUser #optionLogIn-login-menu');
    self.menuClubAvios = element(by.id('menuClubAvios'));
    self.userInput = element(by.id('user'));
    self.passInput = element(by.id('passwd'));
    self.rememberMe = $('.otherLinks label');
    self.btnLogin = $('#menuClubAvios .column .mv_button');

    self.tabSearch = element(by.id('tab-search'));
    self.inputOrigin = $('[class=\'form-group form-group--flight-search\'] .origin input');
    self.inputDestination = $('[class=\'form-group form-group--flight-search\'] .destination input');
    self.btnSearch = element(by.id('btnSubmitHomeSearcher'));
    self.listOptionsVisible = $$('.focused-bottom-arrow .liStation');

    // another pages
    self.errorMsg = element(by.id('errorMsg'));
    self.avail = $('#outboundFlight');

    self.getHomePage = function() {
        browser.get(BASE_URL);
    };

    self.openMenuClubAvios = function() {
        self.btnAccessNoLogedUser.click();
    };

    self.setUser = function(user) {
        self.userInput.sendKeys(user);
    };

    self.setPassword = function(password) {
        self.passInput.sendKeys(password);
    };

    self.clickRememberMe = function() {
        self.rememberMe.click();
    };

    self.clickLogin = function() {
        self.btnLogin.click();
    };

    self.clickHome = function() {
        self.btnHome.click();
    };

    self.setOrigin = function (origin) {
        self.inputOrigin.click();
        self.inputOrigin.clear();
        self.inputOrigin.sendKeys(origin);
        self.listOptionsVisible.get(0).click();
        browser.waitForAngular();
    };

    self.setDestination = function (destination) {
        self.inputDestination.click();
        self.inputDestination.clear();
        self.inputDestination.sendKeys(destination);
        self.listOptionsVisible.get(0).click();
        browser.waitForAngular();
    };

    self.search = function () {
        self.btnSearch.click();
    }

};

module.exports = new HomePage();
