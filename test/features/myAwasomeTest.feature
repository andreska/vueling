@feature1
Feature: feature1

  @MyAwesomeScenario
  Scenario Outline: First Feature

    # ---- Home
    Given Open browser and go to home page
    #When Login with user <email> and pass <pass>
    #When Return to home
    When Set <origin> as origin
    When Set <destination> as destination
    When Search

    # ---- Tickets

    Examples:
      |email                |pass  | origin | destination |
      |acardenas@hiberus.com|123456| Madrid |    París    |
